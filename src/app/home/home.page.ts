import { Component } from '@angular/core';
import { LocalNotifications,ILocalNotificationAction} from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private localNotifications: LocalNotifications)
   {
     this.permission();
     this.haspermission();
     this.hotNotification();
    }

  permission(){
    this.localNotifications.requestPermission().then(ok => console.log(ok));
  }

  haspermission(){
    this.localNotifications.hasPermission().then(ok => console.log(ok))
  }

  hotNotification(){

    console.log('in hotNotification')
    this.localNotifications.schedule(
      {
        title: 'My first notification',
        text: 'Thats pretty easy...',
        foreground: true,
        trigger: {at: new Date(new Date().getTime() + 7200)},
    }
    )
  }

  lanceNotification(){














    
// Schedule delayed notification
this.localNotifications.schedule(
  
  [
    { id: 0, title: 'Design team meeting' },
    { id: 1, summary: 'me@gmail.com', group: 'email', groupSummary: true },
    { id: 2, title: 'Please take all my money', group: 'email', text: 'Copied 2 of 10 files' },
    { id: 3, title: 'A question regarding this plugin', group: 'email' },
    { id: 4, title: 'Wellcome back home', group: 'email' },
    { id: 5, summary: 'her@gmail.com', group: 'email2', groupSummary: true },
    { id: 6, title: 'Please take all my money', group: 'email2', text: 'Copied 2 of 10 files' },
    { id: 7, title: 'A question regarding this plugin', group: 'email2' },
    { id: 8, title: 'Wellcome back home', group: 'email2' }
]

);

  }
}
