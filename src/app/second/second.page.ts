import { Component, OnInit } from '@angular/core';
import { LocalNotifications} from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {

  constructor(private localNotifications: LocalNotifications) {}

  ngOnInit() {
  }

  
  lanceNotification(){

    // Schedule delayed notification
    this.localNotifications.schedule({
      text: 'notification from second',
     
      led: { color: '#FF00FF', on: 500, off: 500 },
      vibrate: true,
     
      foreground: true
    });
    
      }

}
